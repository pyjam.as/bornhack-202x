# Project Ideas

* TOR cloud racing
  - Warm-up a bunch of tor relays before bornhack 2021
  - Put each relay on RPi, hanging from pavilion
  - Let ppl bet (with HAX?) on which relay will move the most data!
* Pixelflut blockchain
  - Why only waste bandwidth, when you can also waste energy?
* TODO: add more projects

# bornhack 2020

ToDo:
 * Find 8 venner, vi skal være 16
 * Village flag og bannere
 * `pyjam.as` stickers

Ting to bring eller køb:
 * Lyskæder
 * FYRTÅRN
 * Flagstang
 * Camping service og glas
 * Store højtalere
 * Tæpper
 * Gummiænder

Køb af bornhack:
 * Køleskabsplads x2
 * 3 x bord (der må meget gerne være ekstra bord plads)
 * 3 stole for meget til gæster
